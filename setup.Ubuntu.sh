# Install docker & docker-compose
apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
apt update
apt -y install docker docker-compose

# Install utilities needed to set up everything
apt -y install apache2-utils git dialog

